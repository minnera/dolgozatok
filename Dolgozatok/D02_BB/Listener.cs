﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D02_BB
{
    class Listener
    {
        private string _name;

        public Listener(string name)
        {
            _name = name;
        }

        public void DoSomething(object o, FoodEventArgs fev)
        {
            //do something
            Console.WriteLine("Empty :(");
        }
    }
}
