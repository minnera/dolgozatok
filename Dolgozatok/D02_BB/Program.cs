﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D02_BB
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Thing> proteins = new List<Thing>();


            Food f1 = new Food(1);

            Drink d1 = new Drink(1);

            proteins.Add(f1);
            proteins.Add(d1);

            Listener l1 = new Listener("Kata");

            f1.Empty += l1.DoSomething;

            for (int i = 0; i < 2; i++)
            {

                Console.WriteLine("Food Előtte: " + proteins[0].Amount);
                Console.WriteLine("Drink Előtte: " + proteins[1].Amount);
                proteins[0].Amount--;
                proteins[1].Amount--;

                Console.WriteLine("Food Utána : " + proteins[0].Amount);
                Console.WriteLine("Drink Utána : " + proteins[1].Amount);
            }


            Console.ReadLine();
        }
    }
}
