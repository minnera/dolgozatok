﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D02_BB
{
    abstract class Thing
    {
        protected int _amount;

        abstract public int Amount
        {
            get;
            set;
        }
    }
}
