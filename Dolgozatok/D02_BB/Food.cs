﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D02_BB
{
    class Food : Thing
    {

        public override int Amount
        {
            get
            {
                return _amount;
            }

            set//0-nál kisebb értéket ne vehessen fel
            {
                if (value >= 0)
                {
                    _amount = value;
                    if(_amount == 0)
                    {
                        //eseményt dobjon fel
                        //ha nincs feliratkozó ne szálljon el
                        if (Empty != null) {
                            FoodEventArgs fea = new FoodEventArgs();
                            Empty(this, fea);
                        }
                    }
                }
                else
                    Console.WriteLine("0-nál nem lehet kisebb!");
            }
        }

        public event EventHandler<FoodEventArgs> Empty;

        public Food()
        {
            _amount = 0;
        }

        public Food(int a)
        {
            _amount = a;
        }

    }
}
