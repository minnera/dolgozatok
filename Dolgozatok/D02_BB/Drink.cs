﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D02_BB
{
    class Drink : Thing
    {

        public override int Amount
        {
            get { return _amount; }
            set
            {
                if (value >= 0)
                {
                    _amount = value;
                }
                else
                    Console.WriteLine("0-nál nem lehet kisebb!");
            }
        }
        public Drink()
        {
            _amount = 0;
        }

        public Drink(int a)
        {
            _amount = a;
        }
    }
}
