﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace D03_BB
{
    class Program
    {
        static void Main(string[] args)
        {
            DataManager dm = new DataManager();

            Console.WriteLine("Adja meg a fájl nevét: (*.xml)");
            string filename = Console.ReadLine();
            int db = 3;

            if (File.Exists(filename))
            {
                Console.WriteLine("A fájl létezik.");
                Animal[] anArray = dm.ReadData(filename);
                Console.WriteLine("A beolvasott tömböt kiírjuk:");
                foreach (Animal a in anArray)
                {
                    Console.WriteLine("Állat neve : {0}, fajtája : {1}, születési dátuma : {2}", a.Name, a.Species, a.BirthDate);
                }
            }
            else
            {
                Animal[] animalArray = new Animal[db];
                Console.WriteLine("A fájl nem létezik.");
                Console.WriteLine("{0} állatot fogunk kérni.", db);
                for(int i = 0; i < db; i++)
                {
                    
                    Console.WriteLine("Adja meg a(z) {0}. állat nevét:", i);
                    string name = Console.ReadLine();
                    Console.WriteLine("Adja meg a(z) {0}. állat fajtáját:", i);
                    string species = Console.ReadLine();
                    Console.WriteLine("Adja meg a(z) {0}. állat születési dátumát ####.##.##:", i);
                    string birthDate = Console.ReadLine();

                    animalArray[i] = new Animal(name, species, birthDate);
                }
                using (FileStream fs = File.Create(filename)) { }
                Console.WriteLine("A fájl létrehozva.");
                dm.WriteData(filename, animalArray);
                Console.WriteLine("A fájlba írás megtörtént.");
            }


            Console.ReadLine();
        }
    }
}
