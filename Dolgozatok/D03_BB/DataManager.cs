﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace D03_BB
{
    class DataManager
    {
        public Animal[] ReadData(string filename)
        {
            if (File.Exists(filename))
            {
                XmlSerializer xs = new XmlSerializer(typeof(Animal[]));
                Animal[] animalArray = null;
                using (FileStream fs = File.OpenRead(filename))
                {
                    animalArray = xs.Deserialize(fs) as Animal[];
                }
                return animalArray;
            }
            else
            {
                Console.WriteLine("Nem létezik ilyen nevű fájl. Null-t adunk vissza.");
                return null;
            }
        }

        public void WriteData(string filename, Animal[] animalArray)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Animal[]));

            using(FileStream fs = File.Create(filename))
            {
                
                 xs.Serialize(fs, animalArray);
                
            }


            //if (File.Exists(filename))
            //{
            //    File.Delete(filename);
            //}
            //else
            //{

            //}
        }
    }
}
