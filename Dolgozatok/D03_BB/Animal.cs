﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D03_BB
{
    public class Animal
    {
        private string _name;

        private DateTime _birthDate;

        private string _species;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public DateTime BirthDate
        {
            get
            {
                return _birthDate;
            }

            set
            {
                _birthDate = value;
            }
        }

        public string Species
        {
            get
            {
                return _species;
            }

            set
            {
                _species = value;
            }
        }

        public Animal()
        {
            _name = "Ismeretlen";
            _birthDate = DateTime.Today;
            _species = "Ismeretlen";
        }

        public Animal(string name)
        {
            _name = name;
            _birthDate = DateTime.Today;
            _species = "Ismeretlen";
        }
        public Animal(string name, string species)
        {
            _name = name;
            _birthDate = DateTime.Today;
            _species = species;
        }
        public Animal(string name, string species, string birthdate)
        {
            _name = name;
            try
            {
                _birthDate = DateTime.Parse(birthdate);
            }
            catch
            {
                Console.WriteLine("Rossz születési dátum lett megadva, default érték beállítva");
                _birthDate = DateTime.Today;
            }
            _species = species;
        }
    }
}
