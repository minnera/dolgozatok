﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D01_BB
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Adj meg egy számot:");
            int szam;
            try
            {
                szam = int.Parse(Console.ReadLine());

            }
            catch (Exception)
            {
                Console.WriteLine("Ez nem szám volt. A program befejeződik.");
                Console.ReadLine();
                return;
            }
            if (1 > szam || 5 < szam)
            {
                return;
            }
            else//létrehoz tömböt
            {
                Console.WriteLine("Tömb lesz.");
                XY[] tomb = new XY[szam];
                for (int i = 0; i  < szam; i++)
                {
                    tomb[i] = new XY();
                }
                Console.WriteLine("A tömb első eleme, x: " + tomb[0].X + ", y: " + tomb[0].Y);
                //egész tömb kiírása
                //foreach (XY ez in tomb)
                //{
                //    Console.WriteLine("X: " + ez.X + ", Y: " + ez.Y);
                //}
            }
            Console.ReadLine();
        }
    }
}
