﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D01_BB
{
    class XY
    {
        private int _x;
        private int _y;
        static Random rnd = new Random();

        public int X
        {
            get
            {
                return _x;
            }

            set
            {
                _x = value;
            }
        }

        public int Y
        {
            get
            {
                return _y;
            }

            set
            {
                _y = value;
            }
        }
        public XY()
        {
            X = rnd.Next(0,101);
            Y = rnd.Next(0, 101);
        }

        public XY(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
